SET FOREIGN_KEY_CHECKS=0;

DROP USER 'timesheet'@'localhost';
CREATE USER 'timesheet'@'localhost' IDENTIFIED BY 'timesheet';
GRANT USAGE ON *.* TO 'timesheet'@'localhost';
CREATE DATABASE restapi CHARACTER SET utf8 COLLATE utf8_general_ci;
USE restapi;

DROP TABLE IF EXISTS `timesheets`;
CREATE TABLE IF NOT EXISTS `timesheets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(32) DEFAULT NULL COMMENT 'Title',
  `hours` varchar(32) DEFAULT NULL COMMENT 'Hours',
  `date` DATE DEFAULT NULL COMMENT 'Created at',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


GRANT SELECT, INSERT, UPDATE, DELETE ON *.* TO 'timesheet'@'localhost';
# password is timesheet

INSERT INTO timesheets (title, hours, date) VALUES ('My First Title','8',DATE_FORMAT(STR_TO_DATE('13-05-2019','%d-%m-%Y'), '%Y-%m-%d'));
INSERT INTO timesheets (title, hours, date) VALUES ('My Second Title','6',DATE_FORMAT(STR_TO_DATE('14-05-2019','%d-%m-%Y'), '%Y-%m-%d'));
INSERT INTO timesheets (title, hours, date) VALUES ('My Third Title','10',DATE_FORMAT(STR_TO_DATE('15-05-2019','%d-%m-%Y'), '%Y-%m-%d'));

SET FOREIGN_KEY_CHECKS=1;
