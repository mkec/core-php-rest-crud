<?php
/**
 * Bootstrap file
 * 
 * PHP version 7.2
 * 
 * @category Description
 * @package  Example
 * @author   Miloš Kecman <milos.kecman@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  Release: 0.1
 * @access   public
 * @link     localhost
 */
define('DS', DIRECTORY_SEPARATOR);

spl_autoload_register(
    function ($class) {
        // Correct DIRECTORY_SEPARATOR
        $class = str_replace(array( '\\', '/' ), DIRECTORY_SEPARATOR, __DIR__.DIRECTORY_SEPARATOR.$class.'.php');
        // Get file real path
        if (false === ( $class = realpath($class) ) ) {
            // File not found
            return false;
        } else {
            include_once $class;
            return true;
        }
    }
);

require_once 'config/conf.php';
require_once '../app/v1/Resource.php';