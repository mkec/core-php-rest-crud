<?php

/**
 * Configuration file
 * 
 * PHP version 7.2
 * 
 * @category Description
 * @package  REST
 * @author   Miloš Kecman <milos.kecman@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  Release: 0.1
 * @access   public
 * @link     localhost
 */
namespace library;
Config::write('db.provider', 'PDO');
//Config::write('db.provider', 'MySQLi');
Config::write('db.host', '127.0.0.1');
Config::write('db.port', 3306);
Config::write('db.databasename', 'restapi');
Config::write('db.user', 'timesheet');
Config::write('db.password', 'timesheet');
