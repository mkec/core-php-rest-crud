<?php
/**
 * Index file
 *
 * PHP version 7.2
 *
 * @category Description
 * @package  REST
 * @author   Miloš Kecman <milos.kecman@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  Release: 0.1
 * @access   public
 * @link     localhost
 */
require_once '../Bootstrap.php';

$dbConfig = new library\DatabaseConfiguration(
    library\Config::read('db.provider'),
    library\Config::read('db.host'),
    library\Config::read('db.port'),
    library\Config::read('db.databasename'),
    library\Config::read('db.user'),
    library\Config::read('db.password')
);

$dbConn = new library\DatabaseConnection($dbConfig);

$request = new library\Request();

if (!$request->isSupported() && $request->method !== "GET") {
    header('Content-Type: application/json; charset=utf8');
    echo '[{"success":"false","message":"Wrong Content - Type!"}]';
    return;
}
if ($request->isMultiPartFormData()) {
    header('Content-Type: application/json; charset=utf8');
    echo '[{"success":"false","message":"Not supported for current method! Please use "application/x-www-form-urlencoded" OR "application/json" "}]';
    return;
}
new app\v1\Resource($request, $dbConn);
