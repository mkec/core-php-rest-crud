<?php
/** phpcs:ignore */
namespace app\v1;

/**
 * RESOURCE handler
 *
 * This class is manipulating with data CRUD using RESTfull logic
 *
 * PHP version 7.2
 *
 * @category Description
 * @package  REST
 * @author   Miloš Kecman <milos.kecman@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  Release: 0.1
 * @access   public
 * @link     localhost
 */
class Resource
{
    private $_conn = null;
    private $_dbconn = null;
    private $_provider = null;
    private $_post = null;
    private $_put = null;
    private $_delete = null;
    private $_parameters = null;
    private $_formParameters = null;
    private $_isJSON = null;

    public $method = null;
    /**
     * Constructor function
     *
     * @param library\Request            $request Request resource
     * @param library\DatabaseConnection $dbcon   Database connection
     */
    public function __construct($request, $dbcon)
    {
        
        $this->method =$request->method;
        $request->parseParams();
        
        $this->_post = $request->post;
        $this->_put = empty($request->put)? $request->formParameters : $request->put;
        $this->_delete = empty($request->delete)?
            (empty($request->put)? (empty($request->formParameters)?
                $request->post : $request->formParameters) : $request->put ) : $request->delete;
        $this->_parameters = $request->parameters;
        $this->_formParameters = $request->formParameters;
        $this->_provider = $dbcon->getProvider();
        $this->_dbconn = $dbcon;
        $this->_isJSON = $request->isJSON;

        $this->executeCRUD();
    }

    /**
     * Execute operation on resource function
     *
     * @return void
     */
    public function executeCRUD()
    {
        /**
         * GET:
         * No params - Get all resource
         * date param - Get resources for specific date
         *
         * POST:
         * JSON - Create resource
         * HTML FORMS - Also create resources
         *
         * PUT:
         * JSON WITH resource ID - update row
         *
         * DELETE:
         * JSON WITH resource ID   - delete row with specific ID
         * JSON WITH resource date - deletes ALL rows with soecific date
         */
        switch ($this->method) {
            case 'GET':
                $this->read();
                break;
            case 'POST':
                $this->create();
                break;
            case 'PUT':
                $this->update();
                break;
            case 'DELETE':
                $this->delete();
                break;
            default:
        }
    }

    /**
     * Read function Prepares
     *  - SQL query for SELECT statement
     *  - Parameters for statement
     *  - Call function for execution
     *
     * @return void
     */
    protected function read()
    {
        // First we check if URL have parameters
        // and if parameter is ID
        // if ID is set we are getting data for that resource only
        if (isset($this->_parameters['date'])) {
            if ($this->_provider === 'MySQLi') {
                $select = <<<EOL
            SELECT id, title, hours, date FROM `timesheets` 
            WHERE DATE_FORMAT(STR_TO_DATE(?,'%d-%m-%Y'), '%Y-%m-%d') = date
EOL;
                $bind = array("?"=>$this->_parameters['date']);
            } else {
                $select = <<<EOL
            SELECT id, title, hours, date FROM `timesheets` 
            WHERE DATE_FORMAT(STR_TO_DATE(:date,'%d-%m-%Y'), '%Y-%m-%d') = date
EOL;
                $bind = array(":date"=>$this->_parameters['date']);
            }
            $this->_executeRead(trim($select), $bind);
        } else {
            $select = <<<EOL
            SELECT id, title, hours, date FROM `timesheets`
EOL;
            $bind = array();
            $this->_executeRead(trim($select), $bind);
        }
    }

    /**
     * Create function Prepares
     *  - SQL query for INSERT statement
     *  - Parameters for statement
     *  - Call function for execution
     *
     * @return void
     */
    protected function create()
    {
        // First we check if POST have all parameters
        if (isset($this->_post['title']) && isset($this->_post['hours']) && isset($this->_post['date'])) {
            if ($this->_provider === 'MySQLi') {
                $insert = <<<EOL
                INSERT INTO `timesheets` 
                (`title`,`hours`,`date`) VALUES 
                (?,?,DATE_FORMAT(STR_TO_DATE(?,'%d-%m-%Y'), '%Y-%m-%d'))
EOL;
                $bind = array(0=>$this->_post['title'],1=>$this->_post['hours'],2=>$this->_post['date']);
            } else {
                $insert = <<<EOL
                INSERT INTO `timesheets` 
                (`title`,`hours`,`date`) VALUES 
                (:title,:hours,DATE_FORMAT(STR_TO_DATE(:date,'%d-%m-%Y'), '%Y-%m-%d'))
EOL;
                $bind = array(
                    ":title"=>$this->_post['title'],
                    ":hours"=>$this->_post['hours'],
                    ":date"=>$this->_post['date']
                );
            }
            $this->_executeCreate(trim($insert), $bind);
        } else {
            header('Content-Type: application/json; charset=utf8');
            echo '{"success":"false","message":"Please check your headers!"}';
            die();
        }
    }

    /**
     * Update function Prepares
     *  - SQL query for UPDATE statement
     *  - Parameters for statement
     *  - Call function for execution
     *
     * @return void
     */
    protected function update()
    {
        // First we check if POST have all parameters
        if (isset($this->_put['id'])
            && isset($this->_put['title'])
            && isset($this->_put['hours'])
            && isset($this->_put['date'])
        ) {
            if ($this->_provider === 'MySQLi') {
                $update = <<<EOL
                UPDATE `timesheets` 
                SET 
                title=?, 
                hours=?, 
                date=DATE_FORMAT(STR_TO_DATE(?,'%d-%m-%Y'), '%Y-%m-%d')
                WHERE id = ?
EOL;
                $bind = array(
                    0=>$this->_put['title'],
                    1=>$this->_put['hours'],
                    2=>$this->_put['date'],
                    3=>$this->_put['id']
                );
            } else {
                $update = <<<EOL
                UPDATE `timesheets` 
                SET 
                title=:title, 
                hours=:hours, 
                date=DATE_FORMAT(STR_TO_DATE(:date,'%d-%m-%Y'), '%Y-%m-%d')
                WHERE id = :id
EOL;
                $bind = array(
                    ":title"=>$this->_put['title'],
                    ":hours"=>$this->_put['hours'],
                    ":date"=>$this->_put['date'],
                    ":id"=>$this->_put['id']
                );
            }
            $this->_executeUpdate(trim($update), $bind);
        } else {
            header('Content-Type: application/json; charset=utf8');
            echo '{"success":"false","message":"Please check your headers!"}';
            die();
        }
    }

    /**
     * Delete function Prepares
     *  - SQL query for DELETE statement
     *  - Parameters for statement
     *  - Call function for execution
     *
     * @return void
     */
    protected function delete()
    {
        // First we check if POST have all parameters
        if (isset($this->_delete['id'])) {
            if ($this->_provider === 'MySQLi') {
                $delete = <<<EOL
                DELETE FROM `timesheets` WHERE id =?
EOL;
                $bind = array(0=>$this->_delete['id']);
            } else {
                $delete = <<<EOL
                DELETE FROM `timesheets` WHERE id =:id
EOL;
                $bind = array(":id"=>$this->_delete['id']);
            }
            $this->_executeDelete(trim($delete), $bind);
        } elseif (isset($this->_delete['date'])) {
            if ($this->_provider === 'MySQLi') {
                $delete = <<<EOL
                DELETE FROM `timesheets` WHERE date = DATE_FORMAT(STR_TO_DATE(?,'%d-%m-%Y'), '%Y-%m-%d')
EOL;
                $bind = array(0=>$this->_delete['date']);
            } else {
                $delete = <<<EOL
                DELETE FROM `timesheets` WHERE date = DATE_FORMAT(STR_TO_DATE(:date,'%d-%m-%Y'), '%Y-%m-%d')
EOL;
                $bind = array(":date"=>$this->_delete['date']);
            }
            $this->_executeDelete(trim($delete), $bind);
        } else {
            header('Content-Type: application/json; charset=utf8');
            echo '{"success":"false","message":"Please check your headers!"}';
            die();
        }
    }

    /**
     * Execute SQL SELECT statement function
     *
     * @param  String $select    SQL string for execution
     * @param  Array  $bindArray Associative array of elements for prepared stmt
     *                           Example: array array(":date"=>"01-10-1991",...);
     * @return void
     */
    private function _executeRead($select, $bindArray)
    {
        if ($this->_provider === 'PDO') {
            try {
                $this->_conn = $this->_dbconn->getConnection();
                $this->_conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                //Dont change int type into string during JSON encoding
                $this->_conn->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
                $this->_conn->setAttribute(\PDO::ATTR_STRINGIFY_FETCHES, false);

                $stmt = $this->_conn->prepare($select);
                if (!empty($bindArray)) {
                    $stmt->execute($bindArray);
                } else {
                    $stmt->execute();
                }
                // set the resulting array to associative
                $results = $stmt->setFetchMode(\PDO::FETCH_ASSOC);
                header('Content-Type: application/json; charset=utf8');
                echo $json = json_encode($stmt->fetchAll());
                $this->_conn = null;
                die();
            } catch (\PDOException $e) {
                echo "Error: " . $e->getMessage();
                die();
            }
        }
        if ($this->_provider === 'MySQLi') {
            try {
                $this->_conn = $this->_dbconn->getConnection();
                $stmt = $this->_conn->prepare($select);

                if (!empty($bindArray)) {
                    // String of input data types for prepared stmt
                    // Ex: $stmt->bind_param('is', $int_value, $string_value);
                    $bindStr = '';
                    // create types
                    foreach ($bindArray as $key => $value) {
                        switch (gettype($value)) {
                            case "boolean":
                                $bindStr .= 'i';
                                break;
                            case "integer":
                                $bindStr .= 'i';
                                break;
                            case "double":
                                $bindStr .= 'd';
                                break;
                            case "string":
                                $bindStr .= 's';
                                break;
                            default:
                                $bindStr .= 's';
                        }
                    }
                    // Get only values from assoc array
                    // Prepare to spread for bind_param
                    $bValues = array_values($bindArray);
                    $stmt->bind_param($bindStr, ...$bValues);
                }
                $stmt->execute();
                $result = $stmt->get_result();
                $resultsArray = array();
                while ($row = $result->fetch_assoc()) {
                    $resultsArray[] = $row;
                }
                // close connection
                $this->_conn->close();
                header('Content-Type: application/json; charset=utf8');
                echo $json = json_encode($resultsArray);
                die();
            } catch (\MySQLiException $e) {
                echo "Error: " . $e->getMessage();
                die();
            }
        }
    }

    /**
     * Execute SQL INSERT statement function
     *
     * @param  String $insert    SQL string for execution
     * @param  Array  $bindArray Associative array of elements for prepared stmt
     *                           Example: array array(":date"=>"01-10-1991",...);
     * @return void
     */
    private function _executeCreate($insert, $bindArray)
    {
        if ($this->_provider === 'PDO') {
            try {
                $this->_conn = $this->_dbconn->getConnection();
                $this->_conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                
                $stmt = $this->_conn->prepare($insert);
                $returnArr = array();
                
                foreach ($bindArray as $key => $value) {
                    $stmt->bindValue($key, $value);
                    $returnArr[ltrim($key, ':')] = $value;
                }
                $stmt->execute();
                // return data
                $id = $this->_conn->lastInsertId();
                header('Content-Type: application/json; charset=utf8');
                echo $json = json_encode(
                    array_merge(array("id" => $id), $returnArr)
                );
                $this->_conn = null;
                die();
            } catch (\PDOException $e) {
                echo "Error: " . $e->getMessage();
                die();
            }
        }
        if ($this->_provider === 'MySQLi') {
            try {
                $this->_conn = $this->_dbconn->getConnection();
                $stmt = $this->_conn->prepare($insert);
                if (!empty($bindArray)) {
                    // String of input data types for prepared stmt
                    // Ex: $stmt->bind_param('is', $int_value, $string_value);
                    $bindStr = '';

                    $returnArr = array();
                    // create types
                    $res = array();
                    
                    foreach ($bindArray as $key => $value) {
                        if ($key === 0) {
                            $returnArr['title'] = $value;
                        }
                        if ($key === 1) {
                            $returnArr['hours'] = $value;
                        }
                        if ($key === 2) {
                            $returnArr['date'] = $value;
                        }
                        
                        switch (gettype($value)) {
                            case "boolean":
                                $bindStr .= 'i';
                                break;
                            case "integer":
                                $bindStr .= 'i';
                                break;
                            case "double":
                                $bindStr .= 'd';
                                break;
                            case "string":
                                $bindStr .= 's';
                                break;
                            default:
                                $bindStr .= 's';
                        }
                    }
                    // Get only values from assoc array
                    // Prepare to spread for bind_param
                    $bValues = array_values($bindArray);
                    $stmt->bind_param($bindStr, ...$bValues);
                }
                $stmt->execute();
                // return data
                $id = $this->_conn->insert_id;
                header('Content-Type: application/json; charset=utf8');
                echo $json = json_encode(
                    array_merge(array("id" => $id), $returnArr)
                );
                $this->_conn->close();
                die();
            } catch (\MySQLiException $e) {
                echo "Error: " . $e->getMessage();
                die();
            }
        }
    }

    /**
     * Execute SQL UPDATE statement function
     *
     * @param  String $update    SQL string for execution
     * @param  Array  $bindArray Associative array of elements for prepared stmt
     *                           Example: array array(":date"=>"01-10-1991",...);
     * @return void
     */
    private function _executeUpdate($update, $bindArray)
    {
        if ($this->_provider === 'PDO') {
            try {
                $this->_conn = $this->_dbconn->getConnection();
                $this->_conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                
                $stmt = $this->_conn->prepare($update);
                $returnArr = array();
                
                foreach ($bindArray as $key => $value) {
                    $stmt->bindValue($key, $value);
                    $returnArr[ltrim($key, ':')] = $value;
                }
                $stmt->execute();
                // return data
                $id = $this->_conn->lastInsertId();
                header('Content-Type: application/json; charset=utf8');
                echo $json = json_encode(
                    array_merge(array("id" => $id), $returnArr)
                );
                $this->_conn = null;
                die();
            } catch (\PDOException $e) {
                echo "Error: " . $e->getMessage();
                die();
            }
        }
        if ($this->_provider === 'MySQLi') {
            try {
                $this->_conn = $this->_dbconn->getConnection();
                $stmt = $this->_conn->prepare($update);
                if (!empty($bindArray)) {
                    // String of input data types for prepared stmt
                    // Ex: $stmt->bind_param('is', $int_value, $string_value);
                    $bindStr = '';

                    $returnArr = array();
                    // create types
                    $res = array();
                    
                    foreach ($bindArray as $key => $value) {
                        if ($key === 0) {
                            $returnArr['title'] = $value;
                        }
                        if ($key === 1) {
                            $returnArr['hours'] = $value;
                        }
                        if ($key === 2) {
                            $returnArr['date'] = $value;
                        }
                        if ($key === 3) {
                            $returnArr['id'] = $value;
                        }
                        
                        switch (gettype($value)) {
                            case "boolean":
                                $bindStr .= 'i';
                                break;
                            case "integer":
                                $bindStr .= 'i';
                                break;
                            case "double":
                                $bindStr .= 'd';
                                break;
                            case "string":
                                $bindStr .= 's';
                                break;
                            default:
                                $bindStr .= 's';
                        }
                    }
                    // Get only values from assoc array
                    // Prepare to spread for bind_param
                    $bValues = array_values($bindArray);
                    $stmt->bind_param($bindStr, ...$bValues);
                }
                $stmt->execute();
                // return data
                header('Content-Type: application/json; charset=utf8');
                echo $json = json_encode($returnArr);
                $this->_conn->close();
                die();
            } catch (\MySQLiException $e) {
                echo "Error: " . $e->getMessage();
                die();
            }
        }
    }

    /**
     * Execute SQL DELETE statement function
     *
     * @param  String $delete    SQL string for execution
     * @param  Array  $bindArray Associative array of elements for prepared stmt
     *                           Example: array array(":date"=>"01-10-1991",...);
     * @return void
     */
    private function _executeDelete($delete, $bindArray)
    {
        if ($this->_provider === 'PDO') {
            try {
                $this->_conn = $this->_dbconn->getConnection();
                $this->_conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                
                $stmt = $this->_conn->prepare($delete);
                reset($bindArray);
                $name = key($bindArray);
                $stmt->bindValue($name, $bindArray[$name]);
                $stmt->execute();
                $count = $stmt->rowCount();
                // return data
                header('Content-Type: application/json; charset=utf8');
                echo $json = json_encode(
                    array("Total rows deleted: " => $count)
                );
                $this->_conn = null;
                die();
            } catch (\PDOException $e) {
                echo "Error: " . $e->getMessage();
                die();
            }
        }
        if ($this->_provider === 'MySQLi') {
            try {
                $this->_conn = $this->_dbconn->getConnection();
                $stmt = $this->_conn->prepare($delete);
                if (!empty($bindArray)) {
                    // String of input data types for prepared stmt
                    // Ex: $stmt->bind_param('is', $int_value, $string_value);
                    $bindStr = '';
                    // create types
                    $res = array();
                    
                    foreach ($bindArray as $key => $value) {
                        switch (gettype($value)) {
                            case "boolean":
                                $bindStr .= 'i';
                                break;
                            case "integer":
                                $bindStr .= 'i';
                                break;
                            case "double":
                                $bindStr .= 'd';
                                break;
                            case "string":
                                $bindStr .= 's';
                                break;
                            default:
                                $bindStr .= 's';
                        }
                    }
                    // Get only values from assoc array
                    // Prepare to spread for bind_param
                    $bValues = array_values($bindArray);
                    $stmt->bind_param($bindStr, ...$bValues);
                }
                $stmt->execute();
                $count = $stmt->affected_rows;
                // return data
                header('Content-Type: application/json; charset=utf8');
                echo $json = json_encode(
                    array("Total rows deleted: " => $count)
                );
                $this->_conn->close();
                die();
            } catch (\MySQLiException $e) {
                echo "Error: " . $e->getMessage();
                die();
            }
        }
    }
}
