# RESTfull API for Timesheets

Project documentation can be found inside public/docs/REST folder ( http://<host>/docs/REST/html/).
Documentation is generated with phpDox.

Coding standards: Zend and PSR2
Tools: DocBlocker, CodeSniffer

## REQUIREMENTS

| SYSTEM | VERSION |
| ------ | ------- |
| OS Ubuntu | >= 16.04 |
| PHP |  >= 7.2 |
| MySQL |  >= 5.7 |
| Apache |  >= 2.4.4 |
*You will find at the bottom of the page list of all enabled modules for apache and php

## INSTALLATION
  - Copy and past ALL DATA to APACHE folder
  - Set vhosts, hosts...
  - If project is installed successfully documentation will be available on: http://<host>/docs/REST/html/
  - NOTE: .htaccess is ,for the most part, responsible for rooting (redirects all requests (except for docs folder) through index.php)
Currently we have only one resource so simple rooting is enough. 
For more resources index would handle REQUEST_URI, QUERY_STRING...
  - MYSQL SCRIPT for creating user, granting privileges and creating table is inside "db" folder
NOTE: Please check it before you run it! There is DROP USER command!

##  USAGE
### Supported CRUD operations
#### CREATE
#### Request
> POST - Creating data (application/json, application/form-data, multipart/form-data, application/x-www-form-urlencoded)
> string : title, 
> string : hours (ex: 4.5h -> where hours must be less than some number from FE), 
> string : date (format: dd-mm-YYY)
#### Response
```sh
{
    "id": "1",
    "title": "My Title",
    "hours": "4.5",
    "date": "01-10-1991"
}
```
#### READ
#### Request
> GET - Listing data from table ( http://<host>/[api/v1/timesheets[/[?date=01-10-1991]]] )
> no params - lists all data
> date (01-10-1991) - lists all data for specific date

#### Response
```sh
[{
    "id": "1",
    "title": "My Title",
    "hours": "4.5",
    "date": "01-10-1991"
},{
    "id": "2",
    "title": "My Title",
    "hours": "4.5",
    "date": "01-10-1991"
},...]
```
#### UPDATE
#### Request
> PUT - Updating data (application/json, application/x-www-form-urlencoded)
> integer: id,
> string : title, 
> string : hours (ex: 4.5h -> where hours must be less than some number from FE), 
> string : date (format: dd-mm-YYY)
#### Response
```sh
{
    "id": "2",
    "title": "My Title",
    "hours": "4.5",
    "date": "01-10-1991"
}
```
#### DELETE
#### Request
> DELETE - Deleting data (application/json, application/x-www-form-urlencoded)
> integer: id   - delete specific item by id 
> string : date - OR all items by date
#### Response
```sh
{
    "Total rows deleted: ": 0
}
```

## ENVIRONMENT
| SYSTEM | VERSION |
| ------ | ------- |
| OS Ubuntu | >= 16.04 |
| PHP |  >= 7.2 |
| MySQL |  >= 5.7 |
| Apache |  >= 2.4.4 |
### Apache loaded modules:
 core_module (static)
 so_module (static)
 watchdog_module (static)
 http_module (static)
 log_config_module (static)
 logio_module (static)
 version_module (static)
 unixd_module (static)
 access_compat_module (shared)
 alias_module (shared)
 auth_basic_module (shared)
 authn_core_module (shared)
 authn_file_module (shared)
 authz_core_module (shared)
 authz_host_module (shared)
 authz_user_module (shared)
 autoindex_module (shared)
 deflate_module (shared)
 dir_module (shared)
 env_module (shared)
 filter_module (shared)
 > headers_module
 
 mime_module (shared)
 mpm_prefork_module (shared)
 negotiation_module (shared)
 > php7_module (shared)
 
 reqtimeout_module (shared)
 > rewrite_module (shared)
 
 setenvif_module (shared)
 status_module (shared)

### PHP modules
calendar
Core
ctype
curl
date
dom
exif
fileinfo
filter
ftp
gd
gettext
hash
iconv
intl

> json

libxml
mbstring
> mysqli
> mysqlnd

openssl
pcntl
pcre
> PDO
> pdo_mysql

Phar
posix
readline
Reflection
session
shmop
SimpleXML
sockets
sodium
SPL
standard
sysvmsg
sysvsem
sysvshm
tokenizer
wddx
xml
xmlreader
xmlrpc
xmlwriter
xsl
Zend OPcache
zip
zlib