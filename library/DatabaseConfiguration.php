<?php
/** phpcs:ignore */
namespace library;

/**
 * DatabaseConfiguration is sample class for configuring DNS
 *
 * Long Description ...
 *
 * PHP version 7.2
 *
 * @category Description
 * @package  REST
 * @author   Miloš Kecman <milos.kecman@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  Release: 0.1
 * @access   public
 * @link     localhost
 */
class DatabaseConfiguration
{
    public $provider = null;
    private $_host = null;
    private $_port = null;
    private $_database = null;
    private $_username = null;
    private $_password = null;
    /**
     * DNS Configuration constructor function
     *
     * @param string  $provider PDO/MySQLi
     * @param string  $host     Hostname
     * @param integer $port     Port number
     * @param string  $database Database name
     * @param string  $username Username
     * @param string  $password Password
     */
    public function __construct(
        string $provider,
        string $host,
        int $port,
        string $database,
        string $username,
        string $password
    )
    {
        $this->provider = $provider;
        $this->_host = $host;
        $this->_port = $port;
        $this->_database = $database;
        $this->_username = $username;
        $this->_password = $password;
    }
    
    /**
     * Get provider function
     *
     * @return string
     */
    public function getProvider(): string
    {
        return $this->provider;
    }
    /**
     * Get host function
     *
     * @return string
     */
    public function getHost(): string
    {
        return $this->_host;
    }
    /**
     * Get port function
     *
     * @return integer
     */
    public function getPort(): int
    {
        return $this->_port;
    }
    /**
     * Get database function
     *
     * @return string
     */
    public function getDatabase(): string
    {
        return $this->_database;
    }
    /**
     * Get username function
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->_username;
    }
    /**
     * Get passwords function
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->_password;
    }
}
