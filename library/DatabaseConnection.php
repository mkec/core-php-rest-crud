<?php
/** phpcs:ignore */
namespace library;

/**
 * DatabaseConnection is sample class for connecting to DB using DI
 *
 * Long Description ...
 *
 * PHP version 7.2
 *
 * @category Description
 * @package  REST
 * @author   Miloš Kecman <milos.kecman@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  Release: 0.1
 * @access   public
 * @link     localhost
 */

class DatabaseConnection
{
    private $_configuration;
    private static $_singleton;
    /**
     * Constructing database connection
     *
     * Dependency Injection (primitive - without container)
     * Dependency type DatabaseConfiguration
     *
     * @param DatabaseConfiguration $config Configuration from configurable class
     */
    public function __construct(DatabaseConfiguration $config)
    {
        $this->_configuration = $config;
    }
    /**
     * Get provider function
     *
     * @return DatabaseConfiguration provider
     */
    public function getProvider()
    {
        return $this->_configuration->getProvider();
    }
    /**
     * Connection to DB function
     *
     * Singleton pattern - Connection between MySQLi and PDO
     *
     * @return DBConnection singleton
     */
    public function getConnection()
    {
        if (!is_null(self::$_singleton)) {
            return self::$_singleton;
        }

        switch ($this->_configuration->getProvider()) {
            case "PDO":
                try {
                    $dsn = sprintf(
                        'mysql:host=%s;dbname=%s',
                        $this->_configuration->getHost(),
                        $this->_configuration->getDatabase()
                    );
                    self::$_singleton = new \PDO(
                        $dsn,
                        $this->_configuration->getUsername(),
                        $this->_configuration->getPassword()
                    );
                } catch (\PDOException $e) {
                    echo "Connection failed: " . $e->getMessage();
                }
                break;
            case "MySQLi":
                // Create connection
                self::$_singleton = new \mysqli(
                    $this->_configuration->getHost(),
                    $this->_configuration->getUsername(),
                    $this->_configuration->getPassword(),
                    $this->_configuration->getDatabase()
                );
                // Check connection
                if (self::$_singleton->connectError) {
                    die("Connection failed: " . self::$_singleton->connectError);
                }
                break;
        }
        return self::$_singleton;
    }
}
