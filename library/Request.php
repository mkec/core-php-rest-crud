<?php
/** phpcs:ignore */
namespace library;

/**
 * Request handler class
 *
 * This class is handling request methods and data
 * so they can be prepared for RESTfull handling
 * Manipulates with headers and super globals
 * Processing between:
 * application/json, application/x-www-form-urlencoded,
 * application/form-data, multipart/form-data
 *
 * PHP version 7.2
 *
 * @category Description
 * @package  REST
 * @author   Miloš Kecman <milos.kecman@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  Release: 0.1
 * @access   public
 * @link     localhost
 */

class Request
{
    public $isJSON = false;
    public $contentType = null;
    public $method = null;
    public $resource = null;
    public $urlElements = null;

    public $post = null;
    public $put = null;
    public $delete = null;
    public $parameters = null;
    public $formParameters = null;

    /**
     * Initializing data from superglobals
     */
    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->resource = $_SERVER['REQUEST_URI'];
        $this->contentType = (isset($_SERVER['CONTENT_TYPE'])) ? $_SERVER['CONTENT_TYPE'] : null;
        $this->urlElements = (isset($_SERVER['PATH_INFO'])) ? explode('/', $_SERVER['PATH_INFO']) : null;
        $this->post = isset($_POST)? $_POST : null;
        $this->put = isset($_PUT)? $_PUT : null;
        $this->delete = isset($_DELETE)? $_DELETE : null;
        // first of all, pull the GET vars
        if (isset($_SERVER['QUERY_STRING'])) {
            parse_str($_SERVER['QUERY_STRING'], $this->parameters);
        }
    }
    /**
     * Check if some content-type's are supported for specific method
     *
     * @return boolean
     */
    public function isMultiPartFormData()
    {
        if ($this->method === "PUT"
            && (stripos($_SERVER["CONTENT_TYPE"], 'multipart/form-data') === 0
            ||  stripos($_SERVER["CONTENT_TYPE"], 'application/form-data') === 0)
        ) {
            return true;
        }
        return false;
    }

    /**
     * Whitelist of supported content-type's
     *
     * @return boolean
     */
    public function isSupported()
    {
        //supported
        if (isset($_SERVER["CONTENT_TYPE"])  && (stripos($_SERVER["CONTENT_TYPE"], 'application/json') === 0
            || stripos($_SERVER["CONTENT_TYPE"], 'application/x-www-form-urlencoded') === 0
            || stripos($_SERVER["CONTENT_TYPE"], 'application/form-data') === 0
            || stripos($_SERVER["CONTENT_TYPE"], 'multipart/form-data') === 0)
        ) {
            return true;
        }
    }
    /**
     * Public check is data type of JSON
     *
     * @return boolean
     */
    public function isApplicationJSON()
    {
        if (stripos($this->contentType, 'application/json') !== false) {
            $this->isJSON = true;
        } else {
            $this->isJSON = false;
        }
    }
    /**
     * Parsing data from headers
     *
     * @return void
     */
    public function parseParams()
    {
        $this->isApplicationJSON();

        if ($this->isJSON && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->post = json_decode(file_get_contents('php://input'), true);
        } elseif ($this->isJSON && $_SERVER['REQUEST_METHOD'] == 'PUT') {
            $this->put = json_decode(file_get_contents('php://input'), true);
        } elseif ($this->isJSON && $_SERVER['REQUEST_METHOD'] == 'DELETE') {
            $this->delete = json_decode(file_get_contents('php://input'), true);
        } elseif (!$this->isJSON) {
            $body = file_get_contents("php://input");
            parse_str($body, $postvars);
            foreach ($postvars as $field => $value) {
                $this->formParameters[$field] = $value;
            }
        }

        if ($this->getJSONLastError()) {
            header('Content-Type: application/json; charset=utf8');
            echo '{"success":"false","message":"JSON corrupted!"}';
            return true;//die()
        }
    }
    /**
     * Checking if JSON data is corrupted function
     *
     * @return void
     */
    public function getJSONLastError()
    {
        if (json_last_error() !== JSON_ERROR_NONE) {
            return true;
        }
    }
}
