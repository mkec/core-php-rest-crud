<?php
/** phpcs:ignore */
namespace library;

/**
 * Configuration class
 *
 * PHP version 7.2
 *
 * @category Description
 * @package  REST
 * @author   Miloš Kecman <milos.kecman@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  Release: 0.1
 * @access   public
 * @link     localhost
 */
class Config
{
    public static $confArray;
    /**
     * Read from configuration function
     *
     * @param  string $name Name of the configuration key
     * @return string       value for the specific key
     */
    public static function read($name)
    {
        return self::$confArray[$name];
    }

    /**
     * Write to configuration function
     *
     * @param  string $name  Name of the configuration key
     * @param  string $value Value for specific key name
     * @return void
     */
    public static function write($name, $value)
    {
        self::$confArray[$name] = $value;
    }
}
